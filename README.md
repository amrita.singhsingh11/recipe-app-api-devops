# Recipe App API DevOps Starting Point

Installed services
Local
- aws-vault

Used AWS services
- IAM - Manual
- EC2 Bastion Instance - Terraform
- S3 bucket - Manual
- Dynamo DB table - Manual
- ECR repo - Manual, Image - Terraform

## 

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000
